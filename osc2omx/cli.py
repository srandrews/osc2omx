import os, sys, signal
from time import sleep
import subprocess, platform, yaml, threading
from OSC import OSCServer
import socket, struct, logging, types
from collections import deque
import argparse
import psutil

# set up loggin
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


# globals to track state

# media.yml file keys are by 'Set' which corresponds to a TouchOSC page
# TouchOSC page values should be /Set/N where N is 1,2, etc.
set_index = 'Set/1'

# will hold the loaded media.yml data
media = None

# for testing on non-omx platforms
use_vlc = False

# timing for the server
# loops every server_receive_request_timeout seconds
server_receive_request_timeout = 2 

# when idle, the media.yml Set/N: interstitial key value is used to play media
idle_thresh = server_receive_request_timeout * 1
idle = 0

# keep track of subprocesses spawned from threads 
# and provide a mutex for updating proc_queue
proc_queue = deque([])
lock = threading.Lock()

# too much goes wrong, bail
BAIL = 0



def set_hi_pri():
    psutil.Process().nice(-18)

# run subprocess in a thread and call onExit callback when exits normally
# thread blocks on subprocess exit and report its status
def popenAndCall(onExit, command):
    """
    Runs the given args in a subprocess.Popen, and then calls the function
    onExit when the subprocess completes.
    onExit is a callable object, and popenArgs is a list/tuple of args that 
    would give to subprocess.Popen.
    """

    def runInThread(onExit, command):
        global BAIL
        with lock:
            while len(proc_queue):
                # new subprocess being spawned
                # for all prior subprocesses kill them w/in mutex'd critical region
                proc = proc_queue.popleft()
                logger.info(('kill -9 {}').format(proc.pid))
                try:
                    os.kill(proc.pid, signal.SIGKILL)
                except OSError as ose:
                    # in case of some unforseen parallel programming error
                    logger.debug(('{} already dead').format(proc.pid))

            try:
                proc = subprocess.Popen(command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, preexec_fn=set_hi_pri)
            except OSError as ose:
                logger.info("Could not run command {}".format(command));
                BAIL += 1
                raise ose

            # add new subprocess w/in mutex'd critical region
            proc_queue.append(proc)
        # ok, new thread will await subprocess to exit
        logger.debug(('Subprocess run {}').format(proc.pid))
        logger.debug(("Start .wait()'ing on {}").format(proc.pid))
        exit_status = proc.wait()
        logger.debug(("Finished .wait()'ing on {}").format(proc.pid))
        logger.debug(('{} Exit Status {}').format(proc.pid, exit_status))
        # subprocess exited and was not killed by the next one being created
        # so remove exit 0 pid from proc_queue
        with lock:
            if proc in proc_queue:
                proc_queue.remove(proc)

    thread = threading.Thread(target=runInThread, args=(onExit, command))
    thread.start()
    if BAIL > 5: sys.exit(-1)
    return thread


def load_osc_media_defs():
    """
    The TouchOSC client is expected to be structured as having at least one page.
    This page name is 'Set/1'.
    This corresponds to the Set/1 key in the media.yml file.
    The Touch OSC client is expected to have a toggle button array.
    This array is named 'Set/1/Songs'
    """
    global media
    with open('media.yml', 'r') as (ymlfile):
        media = yaml.load(ymlfile)
        logger.debug(media)
    logger.info(('Loading media from {}').format(media['Media']['root']))
    for set_key in media:
        logger.debug(media[set_key])
        for song_key in media[set_key]:
            logger.debug(media[set_key][song_key])


def get_host_ip(system):
    """
    Platform independent (macos, linux) effort to find host IP.
    This works for a host not on an internet routed LAN
    """

    def get_default_gateway_linux():
        """Read the default gateway directly from /proc."""
        with open('/proc/net/route') as (fh):
            for line in fh:
                fields = line.strip().split()
                if fields[1] != '00000000' or not int(fields[3], 16) & 2:
                    continue
                return socket.inet_ntoa(struct.pack('<L', int(fields[2], 16)))

    port = 7110
    # set IP here if a static one is desired
    # otherwise expectation is a DHCP lease for host MAC
    host_ip = None
    if system == 'Darwin':
        host_ip = socket.gethostbyname(socket.gethostname())
    else:
        if not host_ip:
            local_gateway = get_default_gateway_linux()
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            s.connect((local_gateway, 1))
            host_ip = s.getsockname()[0]
            s.close()
    return (host_ip, port)


# TODO differential invokation for audio or video?
def media_player_command(media_file):
    if use_vlc:
        command = ['/Applications/VLC.app/Contents/MacOS/VLC', '--quiet', '--intf', 'macosx', ('{media_file}').format(media_file=media_file), 'vlc://quit']
    else:
        filename, file_extension = os.path.splitext(media_file)
        if file_extension == '.wav':
            command = [ '/usr/bin/omxplayer.bin', '-o', 'local', ('{media_file}').format(media_file=media_file)]
        else:
            command = [ '/usr/bin/omxplayer.bin', '--aspect-mode', 'stretch', '-b', '-n', '-1', ('{media_file}').format(media_file=media_file)]
    return command


# callback for subprocess exit
def media_player_exited(proc):
    logger.info(('media player exited {}').format(proc))


def set_changed(path, tags, args, source):
    # TODO: placeholder for eventual set changes
    logger.info(('OSC Msg {} {} {} {}').format(path, tags, args, source))
    logger.info('Set change')


def handle_osc(path, tags, args, source):
    logger.info(('OSC Msg {} {} {} {}').format(path, tags, args, source))

    # TODO panic message to stop player - would need to toggle idle
    if args[0]:
        print args[0]
        try:
            set_index = path.split('/')[1] + '/' + path.split('/')[2]
            print "*** ",set_index
            song_index = int(path.split('/')[5])
            media_file = os.path.join(media['Media']['root'], media[set_index][song_index])
        except KeyError as ke:
            logger.info(ke)
            media_file = 'nomedia'

        command = media_player_command(media_file)
        try:
            logger.info(('Media {}').format(command))
            popenAndCall(media_player_exited, command)
        except Exception as e:
            logger.info(e)


# when server goes idle, run the interstitial video specified in media.yml
def handle_timeout(self):
    global idle
    logger.debug('Server receive request timeout')
    if not proc_queue:
        idle += server_receive_request_timeout
    else:
        idle = 0
    if idle >= idle_thresh:
        logger.debug(('Server Idle {}s').format(idle))
        try:
            media_file = os.path.join(media['Media']['root'], media[set_index]['interstitial'])
            command = media_player_command(media_file)
            logger.info(('Interstitial {}').format(command))
            popenAndCall(media_player_exited, command)
        except KeyError as ke:
            logger.debug("No interstitial")
        except Exception as e:
            logger.info(e)


def main():

    global use_vlc

    parser = argparse.ArgumentParser(description='OSC2OMX.')
    parser.add_argument('--vlc', action='store_true', help='Use VLC instead')
    parser.add_argument('--nolog', action='store_true', help='Do not log')
    args = parser.parse_args()

    if args.vlc: use_vlc = True

    if args.nolog:
        ch.setLevel(logging.CRITICAL)
        os.system('clear')
        os.system('tput civis')

    server = None
    system = platform.system()
    logger.info(('Running on {}').format(system))
    host_ip, port = get_host_ip(system)
    logger.info(('Point OSC client at {} port {}').format(host_ip, port))

    # load media.yml data into global
    load_osc_media_defs()

    # set up OSC UDPServer
    OSCServer.socket_timeout = server_receive_request_timeout
    server = OSCServer((host_ip, port))
    server.print_tracebacks = True
    server.handle_timeout = types.MethodType(handle_timeout, server)
    server.addMsgHandler('/Set/1', set_changed)
    server.addMsgHandler('/Set/2', set_changed)
    server.addMsgHandler('default', handle_osc)

    def signal_handler(sig, frame):
        logger.info('Closing Server')
        os.system('tput cnorm')
        server.close()
        sys.exit(0)

    signal.signal(signal.SIGINT, signal_handler)
    while True:
        server.handle_request()

    return

if __name__ == '__main__':
    main()
