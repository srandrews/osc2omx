# bootstrap if we need to
try:
        import setuptools  # noqa
except ImportError:
        from ez_setup import use_setuptools
        use_setuptools()

from setuptools import setup, find_packages

classifiers = []

setup( author = 'Rich Andrews'
     , author_email = 'stuart.r.andrews@gmail.com'
     , classifiers = classifiers
     , description = 'osc2omx'
     , name = 'osc2omx'
     , url = ''
     , packages = find_packages()
     , entry_points = { 'console_scripts': [
              'osc2omx = osc2omx.cli:main'
        ]}
     # there must be nothing on the following line after the = other than a string constant
     , version = '1.0.0'
     , install_requires = [ 
          'PyYAML==3.13',
          'pyOSC==0.3.5b5294',
          'psutil==5.4.7'
        ]
     ,dependency_links = [
        ]
     , zip_safe = False
)
